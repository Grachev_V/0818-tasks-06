#define _USE_MATH_DEFINES 
#include <cmath>

class Circle

{
	private:
		double Radius, Ference, Area;
	public:
		Circle()
		{
			Radius = 1.0;
			Ference = 2.0 * M_PI;
			Area = M_PI;
		}
		void setRad(double rad)
		{
			if(rad > 0.0)
			{
				Radius = rad;
				Ference = 2.0 * Radius * M_PI;
				Area = M_PI * Radius * Radius;
			}
		}
		
		void setFer(double fer)
		{
			if(fer > 0.0)
			{
				Ference = fer;
				Radius = Ference / (2.0 * M_PI);
				Area = M_PI * Radius * Radius;
			}
		}
		
		void setArea(double ar)
		{
			if (ar > 0.0)
			{
				Area = ar;
				Radius = Area / (2.0 * M_PI);
				Ference = M_PI * Radius * Radius;
			}
		}
		double getRad()
		{
			return Radius;
		}
		double getFer()
		{
			return Ference;
		}
		double getArea()
		{
			return Area;
		}		
};
